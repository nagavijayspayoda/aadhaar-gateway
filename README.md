# Aadhaar Gateway Application

Aadhar Gateway application serves as gateway application where any webapplication can makes use of aadhaar services.  



### Overview
The aadhaar gateway application is integrated with [Keycloak](http://www.keycloak.org/) server for authentication. Other webapplications trying to access the gateway application needs to provide valid credentials. All the requests are logged into database with aadhar results.

The application demo can be found [here](videos/AadharGatewayDemo.mp4)

### Prerequisites
The aadhaar gateway application requires following services to be running

- [PostgresSQL](https://www.postgresql.org/) database
- [Keycloak](http://www.keycloak.org/)

##### Keycloak Configuration:
- Import `'AadharRealm.json'` (src/main/resources) into Keycloak server.
- Create user with appropriate roles given to the user. In this case `'aadhar-user'` with `'aadhar-gateway-role'`. 

Keycloak configuration video can be found [here](videos/Keycloak.mp4) 

##### Application properties Configuration:

###### Keycloak Configuration 
Following are the properties that needs to be configured based on Keycloak settings. 

###### Keycloak server url
`keycloak.auth-server-url=http://localhost:8080/auth`
###### Realm name for Aaadhar Gateway application 
`keycloak.realm=SpringBootKeycloak`
###### Client name 
`keycloak.resource=aadhar-gateway-app`
`keycloak.public-client=true`
`keycloak.expose-token=true`
`keycloak.cors=true`

###### Keycloak Role 
`keycloak.security-constraints[0].authRoles[0]=aadhar-gateway-role`
###### Url pattern that needs to be protected.
`keycloak.security-constraints[0].securityCollections[0].patterns[0]=/gateway`

###### Datasource Configuration
###### Database JDBC url
`spring.datasource.url=jdbc:postgresql://localhost:5439/postgres`
###### Database username
`spring.datasource.username=postgres`
###### Database password
`spring.datasource.password=postgres`
