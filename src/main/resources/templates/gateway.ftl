<#import "/spring.ftl" as spring>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Aadhar API gateway</title>
    <link rel="stylesheet" href="./aadhaar-api-web-sdk.css">
    <script src="assets/jquery/jquery.js"></script>
    <!-- Bootstrap core CSS -->
    <link href="assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="assets/css/main.css" rel="stylesheet">    
    
</head>
<body>

<!-- Page Content -->
    <div class="container">

      <!-- Jumbotron Header -->
      <header class="jumbotron my-4">
        <h1 class="display-5">Aadhar Gateway application</h1>
        <p class="lead"> Aadhaar Gateway application with following services. </p>
		<ul class="list-group">
		    <li class="list-group-item disabled" data-toggle="tooltip" data-placement="top" title="Under Development"">Aadhaar Authentication - <em>(Under development)</em></li>
		    <li class="list-group-item">eKYC</li>
		    <li class="list-group-item disabled" data-toggle="tooltip" data-placement="top" title="Under Development"">eSign - <em>(Under development)</em></li>
	  	</ul>		        
        <p class="lead"> Click on "Open Gateway" for aadhaar services. (Aadhaar Authentication*, eKYC, eSign*. )</p>
        <p > <em>Currently only eKYC is supported based on http://demo.aadhaarapi.com </em></p>
        
        <input type="button" id="gatewayBtn" value="Open gateway" class="btn btn-primary btn-lg">
        
        <a id="returnLink" href="" class="btn btn-primary btn-lg">Return to application</a>
        
        <p> Click on  "Return to application" to redirect to the caller application.</p>
      </header>
      
      <div class="row">
		<div  id="resultsDiv" class="table-responsive">
		 <h2> Aadhaar Results </h2>
		  <table class="table table-striped">
		    <thead>
		      <tr>
		        <th>Name</th>
		        <th>Gender</th>
		        <th>Pincode</th>
		        <th>Year of Birth</th>
		      </tr>
		    </thead>
		    <tbody>
		      <tr>
		        <td id="name"></td>
		        <td id="gender"></td>
		        <td id="pincode"></td>
		        <td id="yob"></td>
		      </tr>
		    </tbody>
		  </table>
		</div>      
      
      </div>
      

    </div>


<div id="quaggaModal" class="qmodel">
    <div id="quaggaModelContent" class="qmodel-content"></div>
</div>

<script type="application/javascript" src="aadhaar-api-web-sdk.js"></script>
<script type="application/javascript">

	


	$.extend({
		  getUrlVars: function(){
		    var vars = [], hash;
		    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
		    for(var i = 0; i < hashes.length; i++)
		    {
		      hash = hashes[i].split('=');
		      vars.push(hash[0]);
		      vars[hash[0]] = hash[1];
		    }
		    return vars;
		  },
		  getUrlVar: function(name){
		    return $.getUrlVars()[name];
		  }
		});
	
	var returnurl = $.getUrlVar("returnurl");				
	$("#returnLink").attr("href",unescape(decodeURIComponent(returnurl)));	
		

    //Setup gateway
    var gatewayOptions = {
        company_display_name: 'Isha Foundation',
        consent_purpose: 'Client Demo of gateway',
        front_text_color: 'FFFFFF',
        background_color: '2C3E50',
        mobile_email_required: 'Y',
        logo_url: 'https://aadhaarapi.com/wp-content/uploads/2016/04/shield-1.png',
        otp_allowed: 'y',
        fingerprint_allowed: 'y',
        default_device: 'SHP20',
        device_selection_allowed: 'n' //New config added  to control dropdown access
    };

    //Generate gateway transaction at backend via rest API call. [Requires API KEY]
    // --on scuccess pass <<gateway transaction id>> below

    document.getElementById("gatewayBtn").onclick = openGateway;
s    
	
	var callbackReponse = function() {
		 //Post response back to the application.
	    var callbackurl = $.getUrlVar("callbackurl");				
		//console.log("url::: " + unescape(decodeURIComponent(callbackurl)));
	 
	    $.ajax({
            type: "POST",
            url: callbackurl,
            async: false,
            data: JSON.stringify({
	        	"id": null,
	            "applicationOrigin": apporigin,
	            "authorizationContext": appcontext,
	            "aadhaarLocalId": responseJSON.Aadhar_Id,
	            "requestType": "ekyc",
	            "requestedDateTime": responseJSON.time,
	            "responseTime": 0,
	            "responseResult": JSON.stringify(responseJSON),
	            "responseReference": responseJSON.transactionId
    	    }),
            contentType: "application/json",
            complete: function (data) {
            	console.log(data);
           		wait = false;
        	}
	    });
	
	} 

    //open gateway
    function openGateway() {
        //create new gateway transaction
        var myAadhaarGateway = new AadhaarAPIGateway('6c09e437-d0a4-48ab-9cdc-699c3508d8cf', gatewayOptions);
        openAadhaarGateway(myAadhaarGateway);
    }

    /************************************************
     *  Handler functions for different scenarios   *
     ************************************************/

    function handleAadhaarConsentDenied() {
        console.log('Handling consent denial at client end');
        //Handle the case when user denies consent
    }

    function handleAadhaarEKYCSuccess(responseJSON) {
        console.log('Handling EKYC success at client end');
        console.log("Json" + JSON.stringify(responseJSON));
        $("#resultsDiv").show();
        document.getElementById("name").innerHTML= responseJSON.e_Kyc.Poi.Name;
        document.getElementById("gender").innerHTML= responseJSON.e_Kyc.Poi.Gender;
        document.getElementById("pincode").innerHTML= responseJSON.e_Kyc.Poa.pc;
        var date = responseJSON.e_Kyc.Poi.Dob;
        var yearstr = date.substring(date.lastIndexOf("-")+1);
        document.getElementById("yob").innerHTML= yearstr;
        
	    var apporigin = $.getUrlVar("apporigin");

		var appcontext = $.getUrlVar("appcontext");
		var returnurl = $.getUrlVar("returnurl");
		
		$("#returnLink").attr("src",returnurl);
	    
	    $.ajax({
            type: "POST",
            url: "/logtransaction",
            async: false,
            data: JSON.stringify({
	        	"id": null,
	            "applicationOrigin": apporigin,
	            "authorizationContext": appcontext,
	            "aadhaarLocalId": responseJSON.Aadhar_Id,
	            "requestType": "ekyc",
	            "requestedDateTime": responseJSON.time,
	            "responseTime": 0,
	            "responseResult": JSON.stringify(responseJSON),
	            "responseReference": responseJSON.transactionId
    	    }),
            contentType: "application/json",
            complete: function (data) {
            	console.log(data);
           		wait = false;
        	}
	    });
	    
	     var callbackurl = $.getUrlVar("callbackurl");				
		var responseUrl = unescape(decodeURIComponent(callbackurl));
	 
	 	console.log("JSON.stringify(responseJSON)" + JSON.stringify(responseJSON));
	    $.ajax({
            type: "POST",
            url: responseUrl,
            async: false,
            data: JSON.stringify(responseJSON),
            contentType: "application/json",
            complete: function (data) {
            	console.log(data);
           		wait = false;
        	}
	    });
    }

    function handleAadhaarEKYCFailure(errorJSON) {
        console.log('Handling EKYC failure at client end');
        console.log(errorJSON);
        //Handle the case when user denies consent
    }

    function handleAadhaarAUTHSuccess(responseJSON) {
        console.log('Handling AUTH success at client end');
        console.log(responseJSON);
        //Handle the case when AUTH is successful
    }

    function handleAadhaarAUTHFailure(errorJSON) {
        console.log('Handling AUTH failure at client end');
        console.log(errorJSON);
        //Handle the case when user denies consent
    }

    function handleAadhaarOTPFailure(errorJSON){
        console.log('Handling OTP failure cases at client end');
        console.log(errorJSON);
        //Handle OTP failure
        /*
          Check for errorJSON.resultCode
            119 : No email or mobile associated with aadhaar (when Fingerprint is disabled)
            998 : Invalid Aadhaar Number.
            410 : OTP requested more than 3 times for same transaction (when Fingerprint is disabled)
         */
    }

    function handleGatewayError(errorJSON) {
        console.log('Handling Gateway launch failure at client end');
        console.log(errorJSON);
    }

    function handleGatewayTermination() {
        //Do not remove this function, even if not used.
        console.log('Handling Gateway Termination at client end');
    }
    
    $(function(){
        $("#resultsDiv").hide();
    });

</script>
</body>
</html>