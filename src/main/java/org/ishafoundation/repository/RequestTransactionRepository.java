package org.ishafoundation.repository;

import org.ishafoundation.model.RequestData;
import org.springframework.data.repository.CrudRepository;

public interface  RequestTransactionRepository extends CrudRepository<RequestData, Long> {

}
