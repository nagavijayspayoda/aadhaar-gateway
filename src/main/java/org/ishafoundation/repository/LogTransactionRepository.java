package org.ishafoundation.repository;

import org.ishafoundation.model.LogTransaction;
import org.springframework.data.repository.CrudRepository;

public interface  LogTransactionRepository extends CrudRepository<LogTransaction, Long> {

}
