package org.ishafoundation.model;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Lob;

@Entity
public class LogTransaction {
	@Id 
	@GeneratedValue
	private Long id;
	
	private String applicationOrigin;
	
	private String authorizationContext;
	
	private String aadhaarLocalId;
	
	private String requestType;
	
	private String requestedDateTime;
	
	private int responseTime;
	
	//@Lob 
	//@Column(name="responseResult", length=50000)
	@Column(columnDefinition = "text")
	private String responseResult;
	
	private String responseReference;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public LogTransaction applicationOrigin(String applicationOrigin) {
		this.applicationOrigin = applicationOrigin;
		return this;
	}

	public String getApplicationOrigin() {
		return applicationOrigin;
	}

	public void setApplicationOrigin(String applicationOrigin) {
		this.applicationOrigin = applicationOrigin;
	}

	public LogTransaction authorizationContext(String authorizationContext) {
		this.authorizationContext = authorizationContext;
		return this;
	}
	
	public String getAuthorizationContext() {
		return authorizationContext;
	}

	public void setAuthorizationContext(String authorizationContext) {
		this.authorizationContext = authorizationContext;
	}
	
	public LogTransaction aadhaarLocalId(String aadhaarLocalId) {
		this.aadhaarLocalId = aadhaarLocalId;
		return this;
	}
	
	public String getAadhaarLocalId() {
		return aadhaarLocalId;
	}

	public void setAadhaarLocalId(String aadhaarLocalId) {
		this.aadhaarLocalId = aadhaarLocalId;
	}

	public LogTransaction requestType(String requestType) {
		this.requestType = requestType;
		return this;
	}	
	
	public String getRequestType() {
		return requestType;
	}

	public void setRequestType(String requestType) {
		this.requestType = requestType;
	}

	public LogTransaction requestedDateTime(String requestedDateTime) {
		this.requestedDateTime = requestedDateTime;
		return this;
	}	
	
	public String getRequestedDateTime() {
		return requestedDateTime;
	}

	public void setRequestedDateTime(String requestedDateTime) {
		this.requestedDateTime = requestedDateTime;
	}

	public LogTransaction responseTime(int responseTime) {
		this.responseTime = responseTime;
		return this;
	}		
	
	public int getResponseTime() {
		return responseTime;
	}

	public void setResponseTime(int responseTime) {
		this.responseTime = responseTime;
	}

	public LogTransaction responseResult(String responseResult) {
		this.responseResult = responseResult;
		return this;
	}			
	
	public String getResponseResult() {
		return responseResult;
	}

	public void setResponseResult(String responseResult) {
		this.responseResult = responseResult;
	}
	
	public LogTransaction responseReference(String responseReference) {
		this.responseReference = responseReference;
		return this;
	}			
	
	public String getResponseReference() {
		return responseReference;
	}

	public void setResponseReference(String responseReference) {
		this.responseReference = responseReference;
	}

}
