package org.ishafoundation;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AadhaarApiGatewayApp {

    public static void main(String[] args) {
        SpringApplication.run(AadhaarApiGatewayApp.class, args);
    }

}
