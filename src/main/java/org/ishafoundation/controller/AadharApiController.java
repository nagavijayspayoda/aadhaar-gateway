package org.ishafoundation.controller;

import org.ishafoundation.model.LogTransaction;
import org.ishafoundation.model.ResponseData;
import org.ishafoundation.repository.LogTransactionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AadharApiController {

	
	@Autowired
	private LogTransactionRepository logTransactionRepository;
	
	
	@RequestMapping(value = "/logtransaction", method = RequestMethod.POST, consumes=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResponseData> add(@RequestBody LogTransaction input) {
		
		System.out.println("Logged Data:" + input.toString());
		logTransactionRepository.save(input);
		
		LogTransaction loggedData = new LogTransaction();
		ResponseData response = new ResponseData(loggedData, "Saved aadhar log transaction");
		return new ResponseEntity<ResponseData>(response, HttpStatus.OK);
	}

}
