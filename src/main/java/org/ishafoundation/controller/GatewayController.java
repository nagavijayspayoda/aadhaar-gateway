package org.ishafoundation.controller;

import java.util.Map;

import org.ishafoundation.model.RequestData;
import org.ishafoundation.repository.RequestTransactionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class GatewayController {
	

	private static final String EMPTY = "";
	@Autowired
	private RequestTransactionRepository requestTransactionRepository;

	@RequestMapping(value="/gateway", method= {RequestMethod.GET})
	public String gatewayAccess( @RequestParam Map<String, String> queryMap) {
		saveRequestData(queryMap);
		return "gateway";
	}

	private void saveRequestData(Map<String, String> queryMap) {
		RequestData requestData = new RequestData();
		requestData.setName(queryMap.getOrDefault("name", EMPTY));
		requestData.setMobile(queryMap.getOrDefault("mobile", EMPTY));
		requestData.setEmail(queryMap.getOrDefault("email", EMPTY));
		requestData.setAddress(queryMap.getOrDefault("address", EMPTY));
		requestData.setGender(queryMap.getOrDefault("gender", EMPTY));
		requestData.setDateofbirth(queryMap.getOrDefault("dateofbirth", EMPTY));
		requestData.setServiceType(queryMap.getOrDefault("serviceType", EMPTY));
		requestData.setApplicationOrigin(queryMap.getOrDefault("apporigin", EMPTY));
		requestData.setApplicationContext(queryMap.getOrDefault("appcontext", EMPTY));
		
		requestTransactionRepository.save(requestData);
	}
}
